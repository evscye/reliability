## Run `glsh kube setup` first to configure the GKE credentials for each cluster
## See https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/kube/k8s-oncall-setup.md#kubernetes-api-access

## Then you can start a SOCKS5 proxy for the current Kube context with:
## $ ssh kube-proxy:$(kubectl config current-context)
## You can also start it in a new Tmux pane with:
## $ tmux split-window -d -v -l 2 ssh kube-proxy:$(kubectl config current-context)

Host kube-proxy:*
    CanonicalizeHostname yes
    ExitOnForwardFailure yes
    PermitLocalCommand yes
    RemoteCommand printf '%%b' '\r\e[34m[kube]\e[0m SOCKS5 Proxy via \e[36m%h\e[0m opened! Press Enter to disconnect.'; read
    ## No % token for the DynamicForward value yet as of OpenSSH 9.1 :'(
    # DynamicForward FIXME
    # LocalCommand kubectl config set clusters.$(echo "%n" | cut -d: -f2).proxy-url socks5://localhost:FIXME >/dev/null

Host kube-proxy:gke_gitlab-db-benchmarking_*
    Hostname bastion-01-inf-db-benchmarking.c.gitlab-db-benchmarking.internal
    DynamicForward 1080
    LocalCommand kubectl config set clusters.$(echo "%n" | cut -d: -f2).proxy-url socks5://localhost:1080 >/dev/null

Host kube-proxy:gke_gitlab-ops_*
    Hostname bastion-01-inf-ops.c.gitlab-ops.internal
    DynamicForward 1081
    LocalCommand kubectl config set clusters.$(echo "%n" | cut -d: -f2).proxy-url socks5://localhost:1081 >/dev/null

# Host kube-proxy:gke_gitlab-pre_*
#     Hostname bastion-01-inf-pre.c.gitlab-pre.internal
#     DynamicForward 1082
#     LocalCommand kubectl config set clusters.$(echo "%n" | cut -d: -f2).proxy-url socks5://localhost:1082 >/dev/null

Host kube-proxy:gke_gitlab-production_*
    Hostname console-01-sv-gprd.c.gitlab-production.internal
    DynamicForward 1083
    LocalCommand kubectl config set clusters.$(echo "%n" | cut -d: -f2).proxy-url socks5://localhost:1083 >/dev/null

Host kube-proxy:gke_gitlab-release_*
    Hostname bastion-01-inf-release.c.gitlab-release.internal
    DynamicForward 1084
    LocalCommand kubectl config set clusters.$(echo "%n" | cut -d: -f2).proxy-url socks5://localhost:1084 >/dev/null

Host kube-proxy:gke_gitlab-staging-1_*
    Hostname console-01-sv-gstg.c.gitlab-staging-1.internal
    DynamicForward 1085
    LocalCommand kubectl config set clusters.$(echo "%n" | cut -d: -f2).proxy-url socks5://localhost:1085 >/dev/null

# Host kube-proxy:gke_gitlab-staging-ref_*
#     Hostname ...
#     DynamicForward 1086
#     LocalCommand kubectl config set clusters.$(echo "%n" | cut -d: -f2).proxy-url socks5://localhost:1086 >/dev/null

Host kube-proxy:gke_gitlab-subscriptions-prod_*
    Hostname bastion-01-inf-prdsub.c.gitlab-subscriptions-prod.internal
    DynamicForward 1087
    LocalCommand kubectl config set clusters.$(echo "%n" | cut -d: -f2).proxy-url socks5://localhost:1087 >/dev/null

Host kube-proxy:gke_gitlab-subscriptions-staging_*
    Hostname bastion-01-inf-stgsub.c.gitlab-subscriptions-staging.internal
    DynamicForward 1088
    LocalCommand kubectl config set clusters.$(echo "%n" | cut -d: -f2).proxy-url socks5://localhost:1088 >/dev/null

Host kube-proxy:gke_gitlab-subscriptions-stg-ref_*
    Hostname bastion-01-inf-stgsub-ref.c.gitlab-subscriptions-stg-ref.internal
    DynamicForward 1089
    LocalCommand kubectl config set clusters.$(echo "%n" | cut -d: -f2).proxy-url socks5://localhost:1089 >/dev/null

# vim:ft=sshconfig:
